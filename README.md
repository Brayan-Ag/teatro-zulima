# Proyecto de Programación: Teatro Zulima

Proyecto de prógramacion Basica 

Miguel Angel Sanabria

Brayan Estiven Aguirre

## Parametros establecidos

Realice el diagrama de flujo y el programa en C para la venta, cancelación de boletos para teatro Zulima, tener en cuenta lo siguientes requerimientos:
1. Los boletos se venden para el mismo día.
2. Un usuario puede cambiar, cancelar y reservar boletos, tantos como sean posible.
3. Se pueden reservar asientos juntos, estos serán 10% más costosos.
4. La sala se divide en zona 1 y zona 2, los boletos de la zona 1 cuestan 10.000 pesos, los de la zona 2 cuesta 10% menos.
5. El programa debe ser diseñado para el vendedor, no para el usuario final, de manera que pueda el vendedor indicar que cierra la atención al usuario y generar las cuentas de la caja.

## Clonación
Instalar PseInt para el pseudocodigo del diagrama de flujo
```bash
yay -S pseint
```
clonar repositorio:
```bash
git clone https://gitlab.com/Brayan-Ag/teatro-zulima.git
```

acceder a la carpeta:
```bash
cd teatro-zulima
```

compilar main.cpp

```bash
g++ -o main main.cpp
```
ejecutar el archivo:

```bash
./main
```
## License

[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International Public License](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode)
