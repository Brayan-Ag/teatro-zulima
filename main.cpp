#include <iostream>
using namespace std;
int f, c, p, venActual = 0;
string teatro[8][8];

int inTeatro() 
{
  for (int i = 0; i < 8; i++) 
  {
    for (int j = 0; j < 8; j++) 
    {
      teatro[i][j] =  "\x1b[42m0 \x1b[0m";
    }
  }
  
  return 0;
}

int mosTeatro() 
{
  cout << "0  1  2  3  4  5  6  7";
  for (int i = 0; i < 8; i++) 
  {
    cout << endl;
    for (int j = 0; j < 8; j++) 
    {
      cout << teatro[i][j] << " ";
    }
    cout << i;
  }
  cout << endl << endl;
  
  return 0;
}

int mosFila(int f) 
{
    for (int j = 0; j < 8; j++) 
    {
      cout << teatro[f][j] << " ";
    }
      cout << "\n1  2  3  4  5  6  7  8";
  cout << endl;
  
  return 0;
}

int venBoleto(int f, int c) 
{
  if (f < 8 && c < 8) 
  {
    if (teatro[f][c] ==  "\x1b[42m0 \x1b[0m") 
    {
        if (f > 3) 
        {
          teatro[f][c] = "\x1b[41mX \x1b[0m";
          venActual += 10000;
          cout << "El boleto fue vendido exitosamente\n";
        }
        else 
        {
          teatro[f][c] = "\x1b[41mX \x1b[0m";
          venActual += 9000;
          cout << "El boleto fue vendido exitosamente\n";
        }
    }
    else 
    {
      cout << "El asiento ya fue vendido\n";
    }
  }
  else 
  {
    cout << "Seleccione un asiento valido\n";
  }
  cout << endl;
  return 0;
}

int venBoletoJ(int f)
{
  string sel;
  while (1) 
  {
  cout << "La fila solicitada es:" << endl;
  mosFila(f);
  cout << endl;
  cout << "0 = Disponible   X = Ocupado" << endl;

  cout << "¿Hay suficientes asientos? [S/N]: ";
  cin >> sel;

  if (sel == "S" || sel == "s") 
  {
    cout << "Ingrese la cantidad de asientos que desea: ";
    cin >> p;
    for (int i = 0; i < p; i++)
      {
        cout << "Digite la columna para el asiento #" << i + 1 << " : ";
        cin >> c;

        if (teatro[f][c] ==  "\x1b[42m0 \x1b[0m")
        {
          teatro[f][c] = "\x1b[41mX \x1b[0m";
          if (f > 3)
          {
            venActual += 11000;
          }
          else
          {
            venActual += 9900;
          }
        }
      }
      cout << "Los boletos se vendieron exitosamente\n";
      break;
  }
  else if (sel == "N" || sel == "n") 
  {
     cout << "Ingrese una fila diferente: ";
     cin >> f;
  }
  else 
    {
    cout << "Ingrese una opcion valida";
    }  
  }

  return 0;
}

int camBoleto() 
{
  teatro[f][c] =  "\x1b[42m0 \x1b[0m";
  if (f > 3) 
  {
    venActual -= 10000;
  }
  else 
  {
    venActual -= 9000;
  }
  cout << "Fila del nuevo asiento: ";
  cin >> f;
  cout << "Columna del nuevo asiento: ";
  cin >> c;
  if (teatro[f][c] ==  "\x1b[42m0 \x1b[0m") 
  {
    if (f > 3) 
    {
      teatro[f][c] = "\x1b[41mX \x1b[0m";
      venActual += 10000;
    }
    else 
    {
      teatro[f][c] = "\x1b[41mX \x1b[0m";
      venActual += 9000;
    }
  }
  else 
  {
    cout << "El asiento seleccionado ya esta ocupado";
  }
  
  return 0;
}

int camBoletoJ(int f)
{
  int p,i,fN,cN;
  string sel;
  while(1)
    {
      cout << "La fila solicitada es: " << endl;
      mosFila(f);
      cout << endl << "0 = Disponible  X = Ocupado" << endl;

      cout << "¿Hay asientos juntos para cambiar S/N: ";
      cin >> sel;
      if(sel == "S" || sel == "s")
      {
        cout << "¿Cuantos asientos desea cambiar: ";
        cin >> p;
        
        for(i = 0; i < p; i++)
          {
            cout << "Digite la columna #" << i+1 << " del asiento que desea cambiar: ";
            cin >> c;
            if(teatro[f][c] == "\x1b[41mX \x1b[0m")
            {
              teatro[f][c] =  "\x1b[42m0 \x1b[0m";
              if(f > 3)
              {
                venActual -= 11000;
              }
              else
              {
                venActual -= 9900;
              }
              cout << "Digite la fila nueva: ";
              cin >> fN;
              cout << endl << "La fila solicitada es: " << endl;
              mosFila(fN);
              cout << endl << "0 = Disponible  X = Ocupado" << endl;
        
              cout << "¿Hay asientos disponibles S/N: ";
              cin >> sel;
              if(sel == "S" || sel == "s")
              {
                cout << "Digite la columna nueva: ";
                cin >> cN;
                if(teatro[fN][cN] ==  "\x1b[42m0 \x1b[0m")
                {
                  teatro[fN][cN] = "\x1b[41mX \x1b[0m";
                  if(fN > 3)
                  {
                    venActual += 11000;
                  }
                  else
                  {
                    venActual += 9900;
                  }
                }
                cout << "El asiento ha sido cambiado exitosamente!" << endl;
              }
              else if(sel == "N" || sel == "n")
              {
                cout << "Digite una fila diferente: ";
                cin >> fN;
              }
              else
              {
                cout << "Digite una opcion valida";
              }
            }
          }
        break;
      }
      else if(sel == "N" || sel == "n")
      {
        cout << "Digite una fila diferente: ";
        cin >> f;
      }
      else
      {
        cout << "Digite una opcion valida";
      }
    }
  
  return 0;
}

int canBoleto() 
{
  cout << "¿Que fila desea cancelar: ";
  cin >> f;
  cout << "¿Que columna desea cancelar: ";
  cin >> c;
  if(teatro[f][c] ==  "\x1b[42m0 \x1b[0m")
  {
    cout << "Este asiento no esta ocupado" << endl << endl;
  }
  else
  {
    teatro[f][c] =  "\x1b[42m0 \x1b[0m";
    if (f > 3) 
    {
      venActual -= 10000;
      cout << "El boleto fue cancelado exitosamente" << endl << endl;
    }
    else 
    {
      venActual -= 9000;
      cout << "El boleto fue cancelado exitosamente" << endl << endl;
    }
  }
  
  return 0;
}

int canBoletoJ(int f)
{
  int p,i;
  string sel;
  while(1)
    {
      cout << "La fila solicitada es:" << endl;
      mosFila(f);
      cout << endl << "0) = Disponible   X = Ocupado" << endl;

      cout << "¿Hay asientos juntos para cancelar S/N: ";
      cin >> sel;
      if(sel == "S" || sel == "s")
      {
        cout << "¿Cuantos asientos juntos desea cancelar: ";
        cin >> p;
        for(i = 0; i < p; i++)
          {
            cout << "Digite la columna para el asiento #" << i+1 << " a cancelar: ";
            cin >> c;

            if(teatro[f][c] == "\x1b[41mX \x1b[0m")
            {
              teatro[f][c] =  "\x1b[42m0 \x1b[0m";
              if(f > 3)
              {
                venActual -= 11000;
              }
              else
              {
                venActual -= 9900;
              }
            cout << "El asiento ha sido cancelado exitosamente!" << endl;
            }
            else
            {
              cout << "Digite un asiento que este ocupado" << endl;
            }
          }
        break;
      }
      else if(sel == "N" || sel == "n")
      {
        cout << "Ingrese una fila diferente: " << endl;
        cin >> f;
      }
      else
      {
        cout << "Ingrese una opcion valida";
      }
    }
  
  return 0;
}

int main() 
{
  inTeatro();
  int op;
  mosTeatro();
  cout << endl;
  while (1) 
  {
  cout << "\n\x1b[96m------Teatro Zulima------\n\x1b[0m";
  cout << "\n\x1b[32m1) Comprar Boleto\n\x1b[0m";
  cout << "\n\x1b[36m2) Cambiar Boleto\n\x1b[0m";
  cout << "\n\x1b[31m3) Eliminar Boleto\n\x1b[0m";
  cout << "\n\x1b[92m4) Comprar Boletos Juntos\n\x1b[0m";
  cout << "\n\x1b[91m5) Eliminar Boletos Juntos\n\x1b[0m";
  cout << "\n\x1b[96m6) Cambiar Boletos Juntos\n\x1b[0m";
  cout << "\n\x1b[35m7) Ver Venta Actual\n\x1b[0m";
  cout << "\n\x1b[37m8) Finalizar Atencion\n\x1b[0m";
  cout << "\n\x1b[96m--------------------------\n\x1b[0m" << ">> ";
  cin >> op;
  
  switch (op) 
  {
    
    case 1:
      cout << "¿Que fila desea: ";
      cin >> f;
      cout << "¿Que Columna desea: ";
      cin >> c;
      venBoleto(f,c);
      break;

    case 2: 
      camBoleto();
      break;

    case 3:
      canBoleto();
      break;

    case 4:
      cout << "¿Que fila desea: ";
      cin >> f;
      venBoletoJ(f);
      break;

      case 5:
      cout << "¿En que fila desea cancelar los asientos: ";
      cin >> f;
      canBoletoJ(f);
      break;
  
      case 6:
      cout << "¿En que fila desea cambiar los asientos: ";
      cin >> f;
      camBoletoJ(f);
      break;
    
    case 7: 
      mosTeatro();
      cout << "La venta actual es: "<< venActual << endl;
      break;
    
    case 8:
      mosTeatro();
      cout << "La venta fue: "<< venActual;
      return 0;
      break;
    }
  }
}